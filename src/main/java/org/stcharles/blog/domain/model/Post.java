package org.stcharles.blog.domain.model;

import jakarta.persistence.*;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "post")
public class Post {
    @Id
    private UUID id;
    @Column(name = "title")
    private String title;
    @Column(name = "publication_date_time")
    private ZonedDateTime publicationDateTime;
    @Column(name = "content")
    private String content;
    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;
//    @ElementCollection
//    private Set<Tag> tags;
    @OneToOne
    @JoinColumn(name = "author_id")
    private User author;
    @OneToMany
    @JoinColumn(name = "post_id")
    private Set<Comment> comments;

    public Post(String title, String content, Category category, Set<Tag> tags, User author) {
        this.title = title;
        this.content = content;
        this.category = category;
//        this.tags = tags;
        this.author = author;
        this.comments = new HashSet<>();
    }

    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public ZonedDateTime getPublicationDateTime() {
        return publicationDateTime;
    }

    public String getContent() {
        return content;
    }

    public Category getCategory() {
        return category;
    }

//    public Set<Tag> getTags() {
//        return Set.copyOf(tags);
//    }

    public User getAuthor() {
        return author;
    }

    public Set<Comment> getComments() {
        return Set.copyOf(comments);
    }

    public void addComment(Comment comment) {
        this.comments.add(comment);
    }
}
