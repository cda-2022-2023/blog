package org.stcharles.blog.domain.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.time.ZonedDateTime;

@Entity
@Table(name = "logged_comment")
public class LoggedComment extends Comment {
    protected LoggedComment(ZonedDateTime dateTime, String content) {
        super(dateTime, content);
    }

    protected LoggedComment() {
        super();
    }
}
