package org.stcharles.blog.domain.model;

import jakarta.persistence.*;

import java.util.Optional;
import java.util.UUID;

@Entity
@Table(name = "category")
public class Category {
    @Id
    private UUID id;
    @Column(name = "name")
    private String name;
    @OneToOne
    @JoinColumn(name = "parent_id")
    private Category parentCategory;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Optional<Category> getParentCategory() {
        return Optional.of(parentCategory);
    }
}
