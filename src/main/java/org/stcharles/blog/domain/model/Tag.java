package org.stcharles.blog.domain.model;

import jakarta.persistence.Embeddable;

@Embeddable
public class Tag {
    private String name;

    public Tag(String name) {
        this.name = name;
    }

    protected Tag() {
    }
}
