package org.stcharles.blog.domain.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "anonymous_comment")
public class AnonymousComment extends Comment {
    @Column(name = "validated")
    private boolean validated;

    public boolean isValidated() {
        return validated;
    }
}
