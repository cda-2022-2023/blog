package org.stcharles.blog.domain.model;

import jakarta.persistence.*;

import java.time.ZonedDateTime;
import java.util.UUID;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "comment")
public abstract class Comment {
    @Id
    private UUID id;
    @Column(name = "publication_date_time")
    private ZonedDateTime publicationDateTime;
    @Column(name = "content")
    private String content;

    protected Comment(ZonedDateTime publicationDateTime, String content) {
        this.publicationDateTime = publicationDateTime;
        this.content = content;
    }

    protected Comment() {
    }

    public UUID getId() {
        return id;
    }

    public ZonedDateTime getPublicationDateTime() {
        return publicationDateTime;
    }

    public String getContent() {
        return content;
    }
}
