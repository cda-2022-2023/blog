package org.stcharles.blog.domain.model;

import jakarta.persistence.*;

import java.util.UUID;

@Entity
@Table(name = "user")
public class User {
    @Id
    private UUID id;
    @Column(name = "login")
    private String login;
    @Column(name = "display_name")
    private String displayName;
    @Column(name = "email")
    private String email;
    @Column(name = "administrator")
    private boolean administrator;

    public User(String login, String displayName, String email, boolean administrator) {
        this.login = login;
        this.displayName = displayName;
        this.email = email;
        this.administrator = administrator;
    }

    protected User() {
    }

    public UUID getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getEmail() {
        return email;
    }

    public boolean isAdministrator() {
        return administrator;
    }
}
