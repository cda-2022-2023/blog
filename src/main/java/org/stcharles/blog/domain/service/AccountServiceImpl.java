package org.stcharles.blog.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.stcharles.blog.data.repository.UserRepository;
import org.stcharles.blog.domain.model.User;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {
    private final UserRepository userRepository;

    @Autowired
    public AccountServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }
}
