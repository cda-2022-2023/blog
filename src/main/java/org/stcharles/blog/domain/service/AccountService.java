package org.stcharles.blog.domain.service;

import org.stcharles.blog.domain.model.User;

import java.util.List;

public interface AccountService {
    List<User> getAllUsers();
}
