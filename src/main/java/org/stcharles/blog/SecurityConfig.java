package org.stcharles.blog;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.stcharles.blog.data.repository.UserRepository;

@Configuration
public class SecurityConfig {
    @Bean
    public UserDetailsManager userDetailsManager(
            PasswordEncoder passwordEncoder,
            UserRepository userRepository
    ) {
        var userDetailsManager = new InMemoryUserDetailsManager();
        var users = userRepository.findAll();

        for (var user : users) {
            var role = user.isAdministrator()
                    ? "ADMIN"
                    : "USER";

            var securityUser = User
                    .withUsername(user.getLogin())
                    .passwordEncoder(passwordEncoder::encode)
                    .password("turlututu")
                    .roles(role)
                    .build();

            userDetailsManager.createUser(securityUser);
        }

        return userDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
