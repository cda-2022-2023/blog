package org.stcharles.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.stcharles.blog.domain.service.AccountService;

@Controller
@RequestMapping("/users")
public class UserController {
    private final AccountService accountService;

    @Autowired
    public UserController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping
    public String index(Model model) {
        var users = accountService.getAllUsers();
        model.addAttribute("users", users);
        return "users/index";
    }
}
