INSERT INTO "user" ("id", "login", "display_name", "email", "administrator")
VALUES (uuid_generate_v4(), 'ron.swanson', 'Duke Silver', 'not@your.business', true),
       (uuid_generate_v4(), 'leslie.knope', 'Future POTUS', 'leslie.knope@parksandrecreation.gov', true),
       (uuid_generate_v4(), 'tom.haverford', 'Swaggy Kitten', 'tom.haverford@parksandrecreation.gov', false);

INSERT INTO "category" ("id", "name", "parent_id")
VALUES (uuid_generate_v4(), 'Informatique', null),
       (uuid_generate_v4(), 'Divers', null);

INSERT INTO "category" ("id", "name", "parent_id")
VALUES (uuid_generate_v4(), 'Dev', (SELECT "id" FROM "category" WHERE name = 'Informatique')),
       (uuid_generate_v4(), 'Free Software', (SELECT "id" FROM "category" WHERE name = 'Informatique'));

INSERT INTO "post" ("id", "author_id", "category_id", "title", "publication_date_time", "content")
VALUES
    (
        uuid_generate_v4(),
        (SELECT "id" FROM "user" WHERE "login" = 'ron.swanson'),
        (SELECT "id" FROM "category" WHERE "name" = 'Divers'),
        'Nothing',
        now(),
        'That''s a free country son, no comment.'
    );