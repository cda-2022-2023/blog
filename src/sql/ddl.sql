CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE "user"
(
    "id" UUID NOT NULL,
    "login" VARCHAR(255) NOT NULL,
    "display_name" VARCHAR(255) NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "administrator" BOOLEAN NOT NULL,

    PRIMARY KEY ("id"),
    UNIQUE ("login"),
    UNIQUE ("display_name"),
    UNIQUE ("email")
);

CREATE TABLE "category"
(
    "id" UUID NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "parent_id" UUID,

    PRIMARY KEY ("id"),
    FOREIGN KEY ("parent_id") REFERENCES category("id")
);

CREATE TABLE "post"
(
    "id" UUID NOT NULL,
    "author_id" UUID NOT NULL,
    "category_id" UUID NOT NULL,
    "title" VARCHAR(255) NOT NULL,
    "publication_date_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    "content" TEXT NOT NULL,

    PRIMARY KEY ("id"),
    FOREIGN KEY ("author_id") REFERENCES "user"("id"),
    FOREIGN KEY ("category_id") REFERENCES "category"("id")
);

CREATE TABLE "comment"
(
    "id" UUID NOT NULL,
    "post_id" UUID NOT NULL,
    "publication_date_time" TIMESTAMP WITH TIME ZONE NOT NULL,
    "content" TEXT NOT NULL,

    PRIMARY KEY ("id"),
    FOREIGN KEY ("post_id") REFERENCES "post"("id"),
);

CREATE TABLE "logged_comment"
(
    "id" UUID NOT NULL,
    "author_id" UUID NOT NULL,

    PRIMARY KEY ("id"),
    FOREIGN KEY ("id") REFERENCES "comment"("id"),
    FOREIGN KEY ("author_id") REFERENCES "user"("id")
);

CREATE TABLE "anonymous_comment"
(
    "id" UUID NOT NULL,
    "validated" BOOLEAN NOT NULL,

    PRIMARY KEY ("id"),
    FOREIGN KEY ("id") REFERENCES "comment"("id")
);
